%% Simulation or Reality?
%
% ME 405 Winter 2021 - Laboratory Assignment #6
%
% *Author:* Kyle Chuang
%
% *Collaborated with:* Enoch Nicholson
%
% California Polytechnic State University, San Luis Obispo, CA
%
% *Date Created:* 2/22/19

%% Constants
rM = 0.06;%[m]
lR = 0.05;%[m]
rB = 0.0105; %[m]
rG = 0.042; %[m]
lP = 0.11; %[m]
rP = 0.0325;%[m]
rC = 0.05;%[m]
mB = 0.03;%[kg]
mP = 0.4;%[kg]
IP = (1.88*10^6)*(1/1000)^3;%[kg*m^2]
IB = .4*mB*rB^2; %[kg*m^2]
b = 10*10^-3; % [N-s-m^2/rad]
g = 9.81; %[m/s^2]

%% Linearization
syms x xd t_y td_y T_x

M11 = -(mB*rB^2 + mB*rC*rB + IB)/rB;
M12 = -(IB*rB + IP*rB + mB*rB^3 + mB*rB*rC^2 + 2*mB*rB^2*rC + mP*rB*rG^2 + mB*rB*x^2)/rB;
M21 = -(mB*rB^2 + IB)/rB;
M22 = -(mB*rB^3 + mB*rC*rB^2 + IB*rB)/rB;

M = [M11 M12
     M21 M22]; 

f1 = b*td_y - g*mB*(sin(t_y)*(rB+rC) + x*cos(t_y)) + T_x*lP/rM + 2*mB*td_y*x*xd - g*mP*rG*sin(t_y);
f2 = -mB*rB*x*td_y^2 - g*mB*rB*sin(t_y);

f = [f1
    f2];

q_dd = M\f;

X_ss = [xd
        td_y
        x
        t_y];

Xdot_ss = [q_dd(1)
           q_dd(2)
           xd
           td_y];
     
dg_dx = jacobian(Xdot_ss, X_ss);

dg_du = jacobian(Xdot_ss, T_x);

%% Open Loop Testing
init = [0, 0, 0, 0]; % [xdot, thetadot, x, theta]
init_u = [0]; % T_x

A = double(subs(dg_dx, [x, t_y, xd, td_y, T_x], [init,init_u]));
B = double(subs(dg_du, [x, t_y, xd, td_y, T_x], [init,init_u]));
C = eye(4);
D = [0];
sys = ss(A,B,C,D);

t = 0:0.01:0.4;
u = zeros(1,41);
u(1) = 1e-3;
% t = 0:0.01:1;
% u = zeros(1,101);
OL = lsim(sys, u, t, init);

figure(1)
subplot(4,1,1)
plot(t, OL(:,3), 'k')
ylabel('X [m]')
xlabel('t [s]')
title('Open Loop Simulation 3d: Impulse of 1mNm-s')

subplot(4,1,2)
plot(t, OL(:,4), 'k')
ylabel('theta_y [rad]')
xlabel('t [s]')

subplot(4,1,3)
plot(t, OL(:,1), 'k')
ylabel('Xdot [m/s]')
xlabel('t [s]')

subplot(4,1,4)
plot(t, OL(:,2), 'k')
ylabel('thetadot_y [rad/s]')
xlabel('t [s]')

%% Closed Loop testing
K = -[0.05 0.02 0.3 0.2];
A_CL = A-B*K;
B_CL = [0
        0
        0
        0];
C_CL = eye(4);
D = [0];
t_CL = 0:0.01:20;
u_CL = zeros(1,2001);
% u_CL(1) = 1e-3;
sys_CL = ss(A_CL, B_CL, C_CL, D);
CL = lsim(sys_CL, u_CL, t_CL, init);

figure(2)
subplot(4,1,1)
plot(t_CL, CL(:,3), 'k')
ylabel('X [m]')
xlabel('t [s]')
title('Closed Loop Simulation 3d: Impulse of 1mNm-s')

subplot(4,1,2)
plot(t_CL, CL(:,4), 'k')
ylabel('theta_y [rad]')
xlabel('t [s]')

subplot(4,1,3)
plot(t_CL, CL(:,1), 'k')
ylabel('Xdot [m/s]')
xlabel('t [s]')

subplot(4,1,4)
plot(t_CL, CL(:,2), 'k')
ylabel('thetadot_y [rad/s]')
xlabel('t [s]')














