# -*- coding: utf-8 -*-
'''
@file       UI3.py
@brief      Lab0x03 Pushing the Right Buttons Front End
@details    This is the front end code that the user interfaces with on Spyder. 
            It consists of two methods: sendChar() and readChar().
            
            *sendChar(): This method asks for the user to input "G", which prompts
            the Nucleo to be ready to accept inputs. If an invalid input is applied,
            an error message is printed and then asks for another input. 
            
            *readChar(): This method waits 5 seconds before waiting for a response
            from the backend code on the Nucleo. The data is parsed into the front end
            as strings which are then stripped and reappended into separate lists. 
            These two lists are then used for plotting a Voltage vs. Time step response.
            Additionally, the data is stored in a .csv file titled "Button Step Response.csv".
            
            Note: If you would like to return multiple plots, please run the front end
            fresh for each plot you would like to receive. The back end only needs to 
            be started once to receive multiple plots. 
            
@author     Kyle Chuang
@date       January 30, 2021
'''
import serial, time
import matplotlib.pyplot as plt
import numpy as np      

# Initialize variables
ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)      

## List for formatted time data         
t = []

## List for formatted voltage data
v = []
    
def sendChar():
    '''
    @brief      Function that prompts for user input.
    @details    Prompts the user to input "G" in order to tell the Nucleo to begin
                accepting inputs. It then writes "G" in ascii, which is read by
                the code on the Nucleo. If an invalid input is applied, an error
                message is printed and then prompts another input. 
    '''
    while True:
        ## "G" input from the user
        inv = input('Type "G" to command the Nucleo to begin waiting for button press: ')
        inpt = ord(inv)
        if inpt != 71:
            print('ERROR! Invalid input!')
        else:
            ser.write(str(inv).encode('ascii'))
            break
    
def readChar():
    '''
    @brief      Function that consolidates the data from the main.py file.
    @details    This method waits 5 seconds before waiting for a response
                from the backend code on the Nucleo. The data is parsed into the front end
                as strings which are then stripped and reappended into separate lists. 
                These two lists are then used for plotting a Voltage vs. Time step response.
                Additionally, the data is stored in a .csv file titled "Button Step Response.csv".
    '''
    # Waits 5 seconds before reading an input. 
    time.sleep(5) 
    
    ## Data read from the back end code
    myval = ser.readline().decode('ascii')
    
    # If there is an input, it will strip and split it into lists for voltage and time
    if myval != 0:
        data = myval.strip('[]\r\n').split('];[')
        
        ## Raw time data read from the back end code
        t_data = data[0].split(',')
        
        ## Raw voltage data read from the back end code
        v_data = data[1].split(',') 
        for i in range(len(t_data)):
            t.append(int(t_data[i]))
            v.append(int(v_data[i]))
            myval = None

    # Plots data and stores it in a .csv file
    plt.plot(t, v)
    plt.xlabel('Time [us]')
    plt.ylabel('Voltage [V]')
    plt.title('Button Step Response')
    a = list(zip(t,v))
    np.savetxt('Button Step Response.csv', a, delimiter = ',')
    

if __name__ == '__main__': 
    sendChar()
    readChar()


ser.close()
