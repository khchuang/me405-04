# -*- coding: utf-8 -*-
'''
@file       tch_driver.py
@brief      Lab0x07
@details    I wrote this code as a driver file for an 8" TFT LCD Touchscreen.
            It contains 4 methods which track the coordinates of any input to 
            the touchscreen:
            
            1. Xscan(): Returns the X coordinate of the input to the touchscreen.
            If there is no X input, then it returns None.
            
            2. Yscan(): Returns the Y coordinate of the input to the touchscreen.
            If there is no Y input, then it returns None. 
            
            3. Zscan(): Returns a boolean output which measures if there is an input
            to the touchscreen. (1 = no input, 0 = input).
            
            4. coord(): Returns the touch input as a coordinate system and the response time.
            Returns in the format: (X, Y, Z), time [us].
            
            Note: Unit system of the length inputs must be consistent!
            
@author     Kyle Chuang
@date       March 2, 2021
'''
import pyb, utime
from pyb import Pin
from pyb import ADC

class touchscreen:
    '''
    @details    I wrote this code as a driver file for an 8" TFT LCD Touchscreen.
                It contains 4 methods which track the coordinates of any input to 
                the touchscreen:
                
                1. Xscan(): Returns the X coordinate of the input to the touchscreen.
                If there is no X input, then it returns None.
                
                2. Yscan(): Returns the Y coordinate of the input to the touchscreen.
                If there is no Y input, then it returns None. 
                
                3. Zscan(): Returns a boolean output which measures if there is an input
                to the touchscreen. (1 = no input, 0 = input).
                
                4. coord(): Returns the touch input as a coordinate system and the response time.
                Returns in the format: (X, Y, Z), time [us].
                
                Note: Unit system of the length inputs must be consistent!
                
    '''
    
    def __init__(self, xp, xm, yp, ym, w, l, xc, yc):
        '''
        @brief      Constructor for the touchscreen driver.
        @param xp   Input for the X+ pin on the FPC breakout board.
        @param xm   Input for the X- pin on the FPC breakout board.
        @param yp   Input for the Y+ pin on the FPC breakout board. 
        @param ym   Input for the Y- pin on the FPC breakout board.
        @param w    Input for the width of the board in centimeters.
        @param l    Input for the length of the board in centimeters.
        @param xc   Input for the X coordinate of the center of the touchscreen.
        @param yc   Input for the Y coordinate of the center of the touchscreen.
        '''
        
        ## An object copy of the X+ pin
        self.xp = xp
        
        ## An object copy of the X- pin
        self.xm = xm
        
        ## An object copy of the Y+ pin
        self.yp = yp
        
        ## An object copy of the Y- pin
        self.ym = ym
        
        ## An object copy of the width of the touchscreen
        self.w = w
        
        ## An object copy of the length of the touchscreen
        self.l = l
        
        ## An object copy of the X coordinate of the center of the touchscreen
        self.xc = xc
        
        ## An object copy of the Y coordinate of the center of the touchscreen
        self.yc = yc
        
        ## Initializes the first X reading for the filter
        self.Xi = 0
        
        ## Initializes the first Y reading for the filter
        self.Yi = 0
    
    def Xscan(self):
        '''
        @brief  Returns the x coordinate of the touch input.
        '''
        # Ground
        pin_xm = Pin(self.xm)
        pin_xm.init(mode=Pin.OUT_PP, value=0)
        
        # Push Pull Output
        pin_xp = Pin(self.xp)
        pin_xp.init(mode=Pin.OUT_PP, value=1)
        
        # Floating
        pin_yp = Pin(self.yp, mode=Pin.IN)
        pin_yp.init(mode=Pin.ANALOG)
        
        # Voltage Reading
        pin_ym = Pin(self.ym, mode=Pin.IN)
        pin_ym.init(mode=Pin.ANALOG)
        self.ADC_ym = ADC(pin_ym)
        
        # Waits for response to settle
        pyb.udelay(5)
        
        # Moving Average Filter code
        Xi1 = self.ADC_ym.read()
        x = ((Xi1 + self.Xi)/2)/4095 
        self.Xi = Xi1
        
        ## Minimum X reading based on the bounds of my specific hardware
        x_min = 0.052
        ## Maximum X reading based on the bounds of my specific hardware
        x_max = 0.923
        
        ## Correction factor for x reading. Previous system underestimated the value.
        x_corr = (x-x_min)/(x_max-x_min)
        
        # Calculates X coordinate
        val = self.l*x_corr - self.xc
        # val = self.l*x - self.xc
        
        # If there is no input to the touchscreen, val = None. The value measured
        # when there is no input ranges between -92 and -93 so this was used 
        # as a threshold. 
        # if val < -0.092:
        #     val = None
        
        return val
    
    def Zscan(self):
        '''
        @brief  Returns a boolean output which measures if there is any input to the touchscreen.
        '''
        # Variable determining if it is touched 
        touch = 1
        
        ## *Ground pins initialized in Xscan()*
        # self.pin_xm = Pin(self.xm)
        # self.pin_xm.init(mode=Pin.OUT_PP, value=0)
        
        # Push Pull Output
        pin_yp = Pin(self.yp)
        pin_yp.init(mode=Pin.OUT_PP, value=1)
        
        # Floating
        pin_xp = Pin(self.xp, mode=Pin.IN)
        pin_xp.init(mode=Pin.ANALOG)
        
        ## *Voltage Reading pins initialized in Xscan()*
        # self.ADC_ym = ADC(self.ym)
        # self.pin_ym = Pin(self.ym, mode=Pin.IN)
        # self.pin_ym.init(mode=Pin.ANALOG)
        # self.ADC_ym = ADC(self.pin_ym)
        
        # Waits for response to settle
        pyb.udelay(5)
        
        # Reads Z 
        val = self.ADC_ym.read()/4095
        
        # If Z is high, then touch = 1
        if val >= 0.99:
            touch = 1
        
        # If Z is low, then touch = 0
        else:
            touch = 0
            
        return touch
    
    
    def Yscan(self):
        '''
        @brief      Returns the y coordinate of the touch input. 
        '''
        # Ground
        pin_ym = Pin(self.ym)
        pin_ym.init(mode=Pin.OUT_PP, value=0)
        
        ## *Push Pull Output Pins initialized in Zscan()*
        # self.pin_yp = Pin(self.yp)
        # self.pin_yp.init(mode=Pin.OUT_PP, value=1)
        
        ## *Floating Pins initialized in Zscan()*
        # self.pin_xp = Pin(self.xp, mode=Pin.IN)
        # self.pin_xp.init(mode=Pin.ANALOG)
        
        # Voltage Reading
        pin_xm = Pin(self.xm, mode=Pin.IN)
        pin_xm.init(mode=Pin.ANALOG)
        ADC_xm = ADC(pin_xm)
        
        # Waits for response to settle
        pyb.udelay(5)
        
        # Filter code
        Yi1 = ADC_xm.read()
        y = ((Yi1 + self.Yi)/2)/4095
        self.Yi = Yi1
        
        ## Minimum Y reading based on the bounds of my specific hardware
        y_min = 0.1
        ## Maximum Y reading based on the bounds of my specific hardware
        y_max = 0.9
        
        ## Correction factor for x reading. Previous system underestimated the value.
        y_corr = (y-y_min)/(y_max-y_min)
        
        # Y coordinate
        val = self.w*y_corr - self.yc
        # val = self.w*y - self.yc
        
        # If there is no input to the touchscreen, val = None. The value measured
        # when there is no input ranges between -59 and -60 so this was used 
        # as a threshold. 
        # if val < -0.059:
        #     val = None
        
        return val

    
    def coord(self):
        '''
        @brief      Returns the coordinates of the touch input and the time it takes to calculate.
        @details    The coordinate system is returned in the format of (X, Y, Z).
                    X and Y will be in the units specified by the given length and 
                    width and Z will be a boolean output which determines whether
                    the touchscreen is being pressed. Z=1 means no input and Z=0 means
                    an input is read. Additionally, this method returns total_time
                    which is the total time it takes to run through the method 
                    returned in microseconds. 
        '''
        
        X = self.Xscan()
        Z = self.Zscan()
        Y = self.Yscan()
        
        # Creates a tuple that calls upon all coordinates
        coord = (X, Y, Z)
        
        return coord

if __name__ == '__main__':
    '''
    Test code
    '''
    xp = Pin.cpu.A0
    xm = Pin.cpu.A6
    yp = Pin.cpu.A7
    ym = Pin.cpu.A1
    w = 0.100 # [m]
    l = 0.176 # [m]
    xc = l/2 # [m]
    yc = w/2 # [m]
    ts = touchscreen(xp, xm, yp, ym, w, l, xc, yc)
    
    # ## CODE FOR TESTING AVERAGE RESPONSE TIME:
    # avg_t = []
    # for i in range(1000):
    #     start_time = utime.ticks_us()
    #     ts.coord()
    #     end_time = utime.ticks_us()
    #     total_time = utime.ticks_diff(end_time, start_time)
    #     avg_t.append(total_time)
    # avg = sum(avg_t)/len(avg_t)
    # print('Average Response Time for 1000 runs: ' + str(avg) + ' us')
    
    ## CODE FOR TRACKING DISPLACEMENT:
    while True:
        print(ts.coord())
    
