# -*- coding: utf-8 -*-
'''
@file main405.py

@mainpage 

@section sec_intro Introduction
Hello, welcome to my online portfolio! On here, I have documented my projects
and various coding assignments from ME 405, Introduction to Mechatronics.

Table of Contents:
    1. Lab 1: TBA
    2. Lab 2: TBA
    3. Lab 3: TBA
    4. Lab 4: TBA
    5. Lab 5: TBA

Source Code Repository: https://bitbucket.org/khchuang/me305-05/src/master/

@page page_TBA Lab 1: TBA

@section sec_sum1 Summary

@details    In this lab, I wrote code to...

Source code can be found here: https://bitbucket.org/khchuang/me305-05/src/master/Lab1/fib.py

@date January 5,2021
'''

