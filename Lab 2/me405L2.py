# -*- coding: utf-8 -*-
'''
@file       me405L2.py
@brief      Labx02: Think Fast!
@details    I wrote this code to test the reaction time of the user. A Nucleo 
            L476RG board is required to facilitate this test. When the code is
            run, 2-3 seconds will pass before the LD2 LED will light up and a 
            timer starts. The timer will end once the user presses the blue USER
            button, and the reaction time will be reported in seconds. If the user does not
            press the button within 1 second of the LED being turned on, the light 
            will automatically be turned off, an error message will print, and 
            the run will not be counted. 
            
            The trials will repeat for as long as the user pleases. Once the 
            user is done, they can exit the program by pressing "Ctrl + c" on 
            the keyboard. This will exit the program and report the average 
            reaction time from all the trials. 
            
@author     Kyle Chuang
@date       January 23, 2021
'''
import pyb, random, micropython

## Creates an emergency buffer to store errors that are thrown inside ISR
micropython.alloc_emergency_exception_buf(200)

## Creates a timer variable. 
tim = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

## Initializes the LED
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

## Pin for blue USER button
pinC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## Variable that tracks number of runs
run_count = 0

## Timer object
timer_count = None

## List of recorded reaction times
rxn_times = []

# Callback function
def myCallback(IQR_source):
    '''
    @brief      Callback function
    @details    This is the callback function that is used when the user clicks
                the blue USER button. This will overrite the code in the while
                True statement, turn off the LED and it will record the time.
    '''
    global timer_count
    timer_count = tim.counter()
    pinA5.low()

## Detects when the USER button is pressed and the callback function is called 
extint = pyb.ExtInt (pinC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, myCallback)   

# The main body of the code. Code will run until Ctrl+c is pressed
while True:
    try:      
        ## Will choose a random number from 2000 (2 seconds) and 3000 (3 seconds). This will be the delay before the LED turns on.
        random_delay = random.randrange(2000,3000)
        pyb.delay(random_delay)
        
        # Clears timer counter for new reaction time test
        tim.counter(0)
        
        #LED on
        pinA5.high()
        
        # Wait 1 second
        pyb.delay(1000)
        
        # LED off
        pinA5.low()
        
        # If a new timer count is recorded, append it to a list and clear the variable.
        if timer_count != None:
            run_count += 1
            rxn_times.append(int(timer_count)/1e6)
            print('Run ' + str(run_count) + ': ' + str(int(timer_count)/1e6) + ' seconds')
            timer_count = None
        # If no new timer is recorded, then print the error statement
        else:
            print('Slow poke!')
    
    # Calculates the average reaction time once ctrl+c is pressed. 
    except KeyboardInterrupt:
        avg = round((sum(rxn_times) / len(rxn_times)), 3)
        print('TEST OVER! Average Reaction Time: ' + str(avg) + ' seconds')
        break
        


