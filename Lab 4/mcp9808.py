# -*- coding: utf-8 -*-
'''
@file       mcp9808.py
@brief      Lab0x04: Hot or Not? mcp9808 driver
@details    This file creates a driver class for a mcp9808 temperature sensor.
            It consists of multiple methods developed by my lab partner Enoch
            for his own personal uses of the mcp9808. For this lab deliverable,
            please pay particular attention to the following methods:
            
                1. get_temp(): which returns the raw temperature value from the 
                mcp9808 sensor in Celsius
                2. celcius(): which calls upon get_temp() to return the ambient 
                temperature in Celsius
                3. fahrenheit(): which calls upon get_temp() to return the ambient
                temperature in Fahrenheit. 
                4. check(): which checks to see if the manufacturer ID for the 
                mcp9808 sensor is correct.
            
            These methods will be used to collect ambient temperature data using 
            the mcp9808 sensor. The sensor will return temperature data in a 
            maximum resolution of .0625 degrees Celsius
            
            The source code for this file can be found here: 
            https://bitbucket.org/ewnichol/me405_shared_work/src/master/me405%20Lab%200x04/mcp9808.py
            
@author Kyle Chuang
@author Enoch Nicholson
@date February 09, 2021
'''
from pyb import I2C
import utime

class MCP9808:
    '''
    @brief      A driver object for the mcp9808 temperature sensor.
    '''
    
    # ---- Definition of Register Adresses ----
    #   ***As seen on page 16 of MCP9808 Datasheet
    
    ##  Configuration register
    R1_CONFIG               = 1
    
    ##  Alert Temperature Upper Boundary Trip register
    R2_UPPER_TEMP           = 2
    
    ##  Alert Temperature Upper Boundary Trip register
    R3_LOWER_TEMP           = 3
    
    ##  Critical Temperature Trip register
    R4_CRIT_TEMP            = 4
    
    ##  Temperature register
    R5_TEMPERATURE          = 5
    
    ##  Manufacturer ID register
    R6_MFG_ID               = 6
    
    ##  Device ID/Revision register
    R7_DEV_ID_AND_REV       = 7
    
    ## Resolution register
    R8_RESOLUTION           = 8
    
    def __init__(self, i2c, addr=0, Tup=0, Tlow=0, Tcrit=0, res=0x03, config=0x0000, debug=False):
        '''
        @brief          Creates a mcp9808 object. 
        @details        This function creates a mcp 9808 object. Unless 
                        specified, all parameters are set to power on defaults.
                        
        @param i2c      The I2C object bound to the bus over which the sensor will communicate.
        @param addr     Interger specifying the user selectable portion of the sensor address (0-7)
        @param Tup      The upper temperature limit for alert in Celcius
        @param Tlow     The lower temperature limit for alert in Celcius
        @param Tcrit    The critical temperature for alert in Celcius
        @param res      The sensor resolution: an int from 0-3 selects from the following [+0.5, +0.25, +0.125, +0.0625]
        @param config   The sensor configuration: config can be set quickly here, or in detail using the config method
        @param debug    A boolean that toggles the trace output on or off.
        '''
        
        # ---- General Setup ----
        
        ##  The I2C adress of the sensor. The adress is created by combining the
        #   four bit manufacturer address with the three bit user selected address.
        self.addr = 0b11000 + addr
        
        ##  A byte array used to recieve data from the sensor.
        self.read_buf = bytearray(2)
        
        ##  The upper temperature limit for alert in Celcius
        self.Tupper = Tup
        
        ##  The lower temperature limit for alert in Celcius
        self.Tlower = Tlow
        
        ##  The critical temperature for alert in Celcius
        self.Tcritical = Tcrit
        
        ##  The ambient temperature in Celcius
        self.Tambinet = 0
        
        ##  The sensor resolution. The value of res indexes the following list 
        #   of available resolutions [+0.5, +0.25, +0.125, +0.0625] (units in Celcius)
        self.res = res
                
        ## The sensor configuration data. See the MCP9808 datasheet for a detailed
        #  explanation of what this specifies.
        self.config = config
        
        ## The manufacturer ID for the MCP9808 sensor
        self.mfg_id = 0x0054
        
        ## The device ID for the MCP9808 sensor
        self.dev_id = 0x04
        
        ## The revision information for the MCP9808 sensor
        self.dev_rev = 0x00
        
        ## Boolean indicating if the sensor is in alert.
        self.alert = False
 
        # ---- Communication ----

        ## The I2C object bound to the bus over which the sensor will communicate.
        self.i2c = i2c
        
        # ---- Debug ----
        
        ## A boolean that toggles the trace output to the command line
        self.debug = debug
        
        if self.debug:
            print('INIT MCP9808 object on I2C bus '+str(self.i2c.bus)+' at address '+str(self.addr))
            
        # ---- Debug ----
        
        # RUN SENSOR CODE / apply settings
    
    def conv_temp(self, Temp, from_unit, to_unit):
        '''
        @brief Converts a temperature to new units
        
        @details This function accepts a numerical temperature and two strings
                 specifying the old and new units. The function works for any
                 combination of the following units:
                     
                     Celcius    - 'C'
                     Fahrenheit - 'F'
                     Kelvin     - 'K'
                     Rankine    - 'R'
                     
        @param Temp         The temperature value to be converted.
        @param from_units   A string specifying the original units.
        @param to_units     A string specifying the new units.
        @return The converted temperature.
        '''
        # Convert temperature to Celcius
        if from_unit == 'C' or from_unit == 'c':
            # print('from c')
            pass
        elif from_unit == 'F' or from_unit == 'f':
            # print('from f')
            Temp = (5/9)*(Temp - 32)
        elif from_unit == 'K' or from_unit == 'k':
            # print('from k')
            Temp = Temp - 273.15
        elif from_unit == 'R' or from_unit == 'r':
            # print('from r')
            Temp = (5/9)*(Temp - 491.67)
        else:
            print('Invalid original units')
        
        # Change to desired units
        if to_unit == 'C' or to_unit == 'c':
            # print('to c')
            return(Temp)
        elif to_unit == 'F' or to_unit == 'f':
            # print('to f')
            return((9/5)*Temp + 32)
        elif to_unit == 'K' or to_unit == 'k':
            # print('to k')
            return(Temp + 273.15)
        elif to_unit == 'R' or to_unit == 'r':
            # print('to r')
            return((9/5)*Temp + 491.67)
        else:
            print('Invalid conversion units')
        
        if self.debug:
            print('     Converting Temp...')
            
    def temp_to_bytes(self, Temp):
        '''
        @brief Converts a temperature in celcius to a bytearry for the MCP9808 sensor
        @details 
        @param Temp The celcius temperature to be converted into bytes
        @return The bytearray representing the given celcius temperature
        '''
        
        # Creates an empty buffer to be filled with the temperature data
        temp_buf = bytearray(2)
        
        # Rounds the temperature to the nearest 0.25 (highest precision for MCP9808 setpoints)
        Temp = round(Temp*4)/4
        
        # Checks and records the sign of the temperature
        if Temp < 0:
            pos = False
            Temp = Temp*-1
        else:
            pos = True
    
        # Separates the temperature into interger and decimal parts
        Temp_int = int(Temp)
        Temp_dec = Temp - Temp_int
        
        # Creates the upper and lower bytes
        UpperByte = Temp_int >> 4
        LowerByte = (Temp_int & 0xF) << 4
        
        # Flips the sign bit if needed
        if pos == False:
            UpperByte = UpperByte ^ 0x10
        else:
            pass
        
        # Flips the bits for the decimal portion of the numebr
        if Temp_dec == 0.25:
            LowerByte = LowerByte ^ 0x04
        elif Temp_dec == 0.50:
            LowerByte = LowerByte ^ 0x08
        elif Temp_dec == 0.75:
            LowerByte = LowerByte ^ 0x0C
        else:
            pass
        
        temp_buf[0] = UpperByte
        temp_buf[1] = LowerByte
        
        return(temp_buf)
    
    def get_temp(self, unit='C'):
        '''
        @brief      Returns the curent ambient temperature measured by the sensor.
        @details    This function returns the curent ambient temerature measurement
                    of the MCO9808 sensor in Celcius, Fahrenheit, Kelvin, or Rankine.
                    
        @param unit A single character str designating the desired units can be 'C' 'F' 'K' or 'R'
        @return The ambient temperature in the desired units.
        '''
        
        # Read temperature register data into the read buffer and split for processing
        self.i2c.mem_read(self.read_buf,self.addr,self.R5_TEMPERATURE)
                
        UpperByte = self.read_buf[0]
        LowerByte = self.read_buf[1]
       
        # print('Bytes')
        # print(UpperByte)
        # print(LowerByte)
        
        # Convert the raw temperature data to a Celcius value 
        #   - The method recommended in the MCP9808 datasheet is used (page 25).
        
        # These conditionals check the flag bits and allow action if needed
        if (UpperByte & 0x80) == 0x80:
            # T_ambient >= T_crit
            self.alert = True
            
        elif (UpperByte & 0x40) == 0x40:
            # T_ambient > T_upper
            self.alert = True
            
        elif (UpperByte & 0x20) == 0x20:
            # T_ambient < T_lower
            self.alert = True
        else:
            self.alert = False
        
        # Flag bits are stripped
        UpperByte = UpperByte & 0x1F
        
        # print('Strip Flags')
        # print(UpperByte)
        # print(LowerByte)
        
        # The sign is checked and the temperature is calculated
        if UpperByte & 0x10 == 0x10:
            # T_ambient < 0
            UpperByte = UpperByte & 0x0F
            Temp = 256 - ((UpperByte*16) + (LowerByte/16))
        else:
            # T_ambient >= 0
            Temp = (UpperByte*16) + (LowerByte/16)
        
        self.Tambinet = Temp
        
        return(self.conv_temp(Temp, 'C', unit))
        
        if self.debug:
            print('     Getting Temp...')
            
    def upper_lim(self, Temp='GET', unit='C'):
        '''
        @brief      Sets or gets the curent upper temperature alert limit of the sensor.
        '''
        
        if Temp == 'GET':
            self.i2c.mem_read(self.read_buf, self.addr, self.R2_UPPER_TEMP)
            UpperByte = self.read_buf[0]
            LowerByte = self.read_buf[1]
            
            if (UpperByte & 0x10) == 0x10:
                # T_upper < 0
                UpperByte = UpperByte & 0x0F
                Temp = -1*((UpperByte << 4) + (LowerByte >> 4))
                
                # Adds decimal temp value
                if (LowerByte & 0x0C) == 0x0C:
                    Temp -= 0.75
                elif (LowerByte & 0x08) == 0x08:
                    Temp -= 0.50
                elif (LowerByte & 0x04) == 0x04:
                    Temp -= 0.25
                else:
                    pass
                
            else:
                # T_upper >= 0
                Temp = (UpperByte << 4) + (LowerByte >> 4)
                
                # Adds decimal temp value
                if (LowerByte & 0x0C) == 0x0C:
                    Temp += 0.75
                elif (LowerByte & 0x08) == 0x08:
                    Temp += 0.50
                elif (LowerByte & 0x04) == 0x04:
                    Temp += 0.25
                else:
                    pass
        else:
            try:
                # Temperature is converted to celcius then into a bytearray
                Temp_C = self.conv_temp(Temp,unit,'C')
                temp_buf = self.temp_to_bytes(self, Temp_C)
                
                self.i2c.mem_write(temp_buf, self.addr, self.R2_UPPER_TEMP)
                
            except:
                print('Unable to set upper temp limit')
            
        
        if self.debug:
            print('Get or set upper temp limit')
            
    def lower_lim(self, Temp='GET', unit='C'):
        '''
        @brief      Sets or gets the curent lower temperature alert limit of the sensor.
        @details    TXT
        '''
        
        if Temp == 'GET':
            self.i2c.mem_read(self.read_buf, self.addr, self.R3_LOWER_TEMP)
            UpperByte = self.read_buf[0]
            LowerByte = self.read_buf[1]
            
            if (UpperByte & 0x10) == 0x10:
                # T_lower < 0
                UpperByte = UpperByte & 0x0F
                Temp = -1*((UpperByte << 4) + (LowerByte >> 4))
                
                # Adds decimal temp value
                if (LowerByte & 0x0C) == 0x0C:
                    Temp -= 0.75
                elif (LowerByte & 0x08) == 0x08:
                    Temp -= 0.50
                elif (LowerByte & 0x04) == 0x04:
                    Temp -= 0.25
                else:
                    pass
                
            else:
                # T_lower >= 0
                Temp = (UpperByte << 4) + (LowerByte >> 4)
                
                # Adds decimal temp value
                if (LowerByte & 0x0C) == 0x0C:
                    Temp += 0.75
                elif (LowerByte & 0x08) == 0x08:
                    Temp += 0.50
                elif (LowerByte & 0x04) == 0x04:
                    Temp += 0.25
                else:
                    pass
        else:
            try:
                # Temperature is converted to celcius then into a bytearray
                Temp_C = self.conv_temp(Temp,unit,'C')
                temp_buf = self.temp_to_bytes(self, Temp_C)
                
                self.i2c.mem_write(temp_buf, self.addr, self.R3_LOWER_TEMP)
                
            except:
                print('Unable to set lower temp limit')
            
        
        if self.debug:
            print('Get or set lower temp limit')
            
    def crit_temp(self, Temp='GET', unit='C'):
        '''
        @brief      Sets or gets the curent critical temperature alert of the sensor.
        '''
        
        if Temp == 'GET':
            self.i2c.mem_read(self.read_buf, self.addr, self.R4_CRIT_TEMP)
            UpperByte = self.read_buf[0]
            LowerByte = self.read_buf[1]
            
            if (UpperByte & 0x10) == 0x10:
                # T_critical < 0
                UpperByte = UpperByte & 0x0F
                Temp = -1*((UpperByte << 4) + (LowerByte >> 4))
                
                # Adds decimal temp value
                if (LowerByte & 0x0C) == 0x0C:
                    Temp -= 0.75
                elif (LowerByte & 0x08) == 0x08:
                    Temp -= 0.50
                elif (LowerByte & 0x04) == 0x04:
                    Temp -= 0.25
                else:
                    pass
                
            else:
                # Tcritical >= 0
                Temp = (UpperByte << 4) + (LowerByte >> 4)
                
                # Adds decimal temp value
                if (LowerByte & 0x0C) == 0x0C:
                    Temp += 0.75
                elif (LowerByte & 0x08) == 0x08:
                    Temp += 0.50
                elif (LowerByte & 0x04) == 0x04:
                    Temp += 0.25
                else:
                    pass
        else:
            try:
                # Temperature is converted to celcius then into a bytearray
                Temp_C = self.conv_temp(Temp,unit,'C')
                temp_buf = self.temp_to_bytes(self, Temp_C)
                
                self.i2c.mem_write(temp_buf, self.addr, self.R4_CRIT_TEMP)
                
            except:
                print('Unable to set critical temp')
            
        
        if self.debug:
            print('Get or set critical temp')
            
    def res(self, res='GET'):
        '''
        @brief      Gets or sets the device measurement resolution.
        @details    This method gets or sets the sampling resolution for the MCP9808
                    temperature sensor. There are four valid resolution settings
                    as outlined by the following:
                        
                        0 = +0.05 C
                        1 = +0.25 C
                        2 = +0.125 C
                        3 = +0.0625 C
                    
        @param res  The resolution index to set, if no value is passed, the current resolution index is returned.
        @return     The current resolution.
        '''
        
        if res == 'GET':
            res_buf = self.i2c.mem_read(1, self.addr, self.R8_RESOLUTION)
            res = res_buf & 0x03
            
            return res
        elif res in (0,1,2,3):
            self.i2c.mem_write(res, self.addr, self.R8_RESOLUTION)
        else:
            print('Unable to set sensor resolution.')
        
        if self.debug:
            print('Getting or setting device resolution')
            
    def get_mfgID(self):
        '''
        @brief      Obtains the Manufacturer ID from the MCP9808 sensor
        @return     An interger representing the Manufacturer ID
        '''
        
        # Reads the Manufacturer ID number from the sensor register. This register is
        #   read only, and the most significant byte does not contain any data.
        self.i2c.mem_read(self.read_buf, self.addr, self.R6_MFG_ID)
        mfg_id = self.read_buf[1]
        
        self.mfg_id = mfg_id
        
        return(mfg_id)
        
        if self.debug:
                print('Getting MFG ID')
            
    def get_devID(self):
        '''
        @brief      Obtains the Device ID and revision information from the MCP9808 sensor
        @return An tuple containing the Device ID and device revision information
        '''
        
        # Reads the Device ID number from the sensor register. This register is
        #   read only with the most significant bit containing the Device ID and
        #   the least significant bit containing the revision number.
        self.i2c.mem_read(self.read_buf, self.addr, self.R7_DEV_ID_AND_REV)
        dev_id = self.read_buf[0]
        dev_rev = self.red_buf[1]
        
        self.dev_id = dev_id
        self.dev_rev = dev_rev
        
        return((dev_id,dev_rev))
        
        if self.debug:
                print('Getting Dev ID')
            
    def config(self, act='GET', HYST=0x00, SHDN=False, CRIT_LOK=False, 
               WIN_LOK=False, INT_CLR=False, ALERT_STAT=False, ALERT_CNT=False,
               ALERT_SEL=False, ALERT_POL=False, ALERT_MOD=False):
        '''
        @brief      Allows reading and writing of the MCP9808 sensor config register
        @details    Method not complete...
        '''
        
        # Not implemented yet
        if act == 'GET':
            self.i2c.mem_read(self.read_buf, self.addr, self.R1_CONFIG)
            
        elif act == 'SET':
            pass
        else:
            pass
        
        if self.debug:
            print('Getting or setting config')
            
    def alert(self):
        '''
        @brief Returns the device alert state
        '''
        
        return self.alert
        
        if self.debug:
            print('Checking alert')
            
    def celcius(self):
        '''
        @brief Returns the measured temperature in degrees Celcius
        @return The the measured temperature in degrees Celcius
        '''
        
        return self.get_temp()
        
    def fahrenheit(self):
        '''
        @brief Returns the measured temperature in degrees Fahrenheit
        @return The the measured temperature in degrees Fahrenheit
        '''
        
        return self.get_temp('F')
        
    def check(self):
        '''
        @brief Checks to see if the device registers the correct Manufacturer ID (0x0054)
        @return The status of the check True for a pass, Flase for a fail
        '''
        
        chk = self.get_mfgID() == 0x0054
        
        return chk
        
        if self.debug:
            print('Checking ID')
        
if __name__ == '__main__':
    '''
    Test code for the MCP9808 sensor driver.
    '''
    
    ## The pyb.I2C object used to communicate with the MCP9808 sensor. The communication
    #   channel is opened on bus 1, and the Nucleo is designated as the master.
    i2c = I2C(1, I2C.MASTER)
    
    print('The available devices addresses on the bus are...')
    print(i2c.scan())
    
    ## The mcp9808 object connected to the external temperature sensor
    temp_sensor = MCP9808(i2c,0)
    
    while True:
        
        ## The ambient temperature read by the MCP9808 sensor in C
        amb_temp = temp_sensor.celcius()
        
        print('''
              Data recorded: 
                  Ambient: {:} C
                  
                  '''.format(amb_temp))
        
        utime.sleep(5)







