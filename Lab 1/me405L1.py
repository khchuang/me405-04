# -*- coding: utf-8 -*-
'''
@file       me405L1.py
@brief      Labx01: Vendotron Simulation
@details    I wrote this code to simulate the functionality of a fictional vending 
            machine, the Vendotron. The code uses a finite state machine to 
            display print statements which represent the actions of a real vending
            machine. On start up, the user is greeted with this message: 
                Thirsty? Grab a cold drink today! 
                  ('c') Cuke      | $2.00
                  ('p') Popsi     | $4.25
                  ('s') Spryte    | $3.75
                  ('d') Dr.Pupper | $6.25
            The user can then enter "money" by pressing numbers on their keyboard:
            0-Penny, 1-Nickel, 2-Dime, 3-Quarter, 4-One Dollar, 5-Five Dollars,
            6-Ten Dollars, 7-Twenty Dollars. Each time a number is pressed, the 
            remaining balance is updated. 
            
            At any time the user can press 'c', 'p','s', or 'd' to make a drink selection. The code then compares the drink
            price to the balance and determines if the purchase can be made. If so,
            "DRINK VENDED" will display, indicating that a drink was dispensed successfully.
            If not, then it will display "INSUFFICIENT FUNDS" with your remaining balance. 
            Leftover balances can be "ejected" by pressing 'e' at any time during
            operation or additional selections can be made by inserting more money 
            into the machine. 
            
            The program will run indefinitely so use 'ctrl + c' to exit whenever
            you are finished with the Vendotron.
            
@author     Kyle Chuang
@date       January 14, 2021
'''
import keyboard

## Variable to indicate which state the system is in
state = 0

# Code provided which helps set up the ability of pressing keys at any time during operation. 
pushed_key = None

def on_keypress (thing):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = thing.name

keyboard.on_press (on_keypress) ###### Set callback


def getChange(price, payment):
    '''
    @brief      Computes change for monetary transaction
    @details    This is my code that takes the expected price and payment amounts
                and returns a tuple with the change expected to be returned in the least
                denominations as possible. The return tuple is formatted as follows:
                
                "(# of pennies, # of nickels, # of dimes, # of quarters, # of one dollar bills, # of five dollar bills, # of ten dollar bills, # of twenty dollar bills)"
    
    @param price    The price of the item to be purchased
    @param payment  The payment balance used to purchase the item. 
    @return         If funds are sufficient, returns a tuple of bills/coins. If insufficient, returns None.
    '''
    
    ## Value of change due
    change_tot = payment - price
    
    # Insufficient payment
    if change_tot < 0:
        # print('Error! Payment total is less than the price!')
        return None
    
    else:
        # print('$' + str(change_tot/100))
          
        ## Number of Twenties back
        twenty_b = change_tot // 2000
        
        ## Intermediate variable that represents the remaining change due
        pr = change_tot - twenty_b*2000
        
        ## Number of Tens back
        ten_b = pr // 1000
        pr = pr - ten_b*1000
        
        ## Number of Fives back
        five_b = pr // 500 
        pr = pr - five_b*500
        
        ## Number of Ones back
        one_b = pr // 100
        pr = pr - one_b*100
        
        ## Number of Quarters back
        q_b = pr // 25 
        pr = pr - q_b*25
        
        ## Number of Dimes back
        d_b = pr // 10 
        pr = pr - d_b*10
        
        ## Number of Nickels back
        n_b = pr // 5 
        pr = pr - n_b*5
        
        ## Number of Pennies back
        p_b = pr
        
        ## Tuple with the number of denominations to return to the user
        change = (p_b, n_b, d_b, q_b, one_b, five_b, ten_b, twenty_b)
    
        # print('Change returned:' + str(change))
    
        return change

def printWelcome():
    print('''Thirsty? Grab a cold drink today! 
          ('c') Cuke      | $2.00
          ('p') Popsi     | $4.25
          ('s') Spryte    | $3.75
          ('d') Dr.Pupper | $6.25
          ''')


while True:
    if state == 0:
        print('''
              Use numpad to "enter money": 
                  0-Penny
                  1-Nickel
                  2-Dime
                  3-Quarter
                  4-One Dollar
                  5-Five Dollars
                  6-Ten Dollars
                  7-Twenty Dollars
              
              Press 'e' at any time to eject the remaining balance.
              ''')
        state = 1
    
    elif state == 1:
        # Prints welcome message and always transitions to the next state
        printWelcome()
        balance = 0
        state = 2
        
    elif state == 2:
        # Accepts user inputs for coins. Takes user input and adds them to a tuple. 
        # if coin inserted, Print Balance
        # Transition when User makes a selection (S2) or eject (S4) is hit
        # User will be chilling here for the majority fo the time. Update balance accordingly. 
        price = 0
        try:
            # If a key has been pressed, check if it's a key we care about
            if pushed_key:
                if pushed_key == "0":
                    balance += 1
                    print ("Penny Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '1':
                    balance += 5
                    print ("Nickel Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '2':
                    balance += 10
                    print ("Dime Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '3':
                    balance += 25
                    print ("Quarter Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '4':
                    balance += 100
                    print ("One Dollar Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '5':
                    balance += 500
                    print ("Five Dollars Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '6':
                    balance += 1000
                    print ("Ten Dollars Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                elif pushed_key == '7':
                    balance += 2000
                    print ("Twenty Dollars Inserted")
                    display_balance = balance/100
                    print ('Balance: ${:.2f}'.format(display_balance))
                    
                # Selection pressed
                elif pushed_key == 'c':
                    print ("DRINK SELECTED: Cuke        $2.00")
                    price = 200
                    state = 3
                    
                elif pushed_key == 'p':
                    print ("DRINK SELECTED: Popsi       $4.25")
                    price = 425
                    state = 3
                    
                elif pushed_key == 's':
                    print ("DRINK SELECTED: Spryte      $3.75")
                    price = 375
                    state = 3
                    
                elif pushed_key == 'd':
                    print ("DRINK SELECTED: Dr.Pupper   $6.25")
                    price = 625
                    state = 3
                
                # Eject
                elif pushed_key == 'e':
                    print ("EJECT")
                    state = 5
                pushed_key = None

        # If Control-C is pressed, this is sensed separately from the keyboard
        # module; it generates an exception, and we break out of the loop
        except KeyboardInterrupt:
            break
        
        # display_balance = balance/1000010000
        # print ('Balance: ${:.2f}'.format(display_balance))
        
    elif state == 3:
        # Selection pressed. Takes tuple and calculates balance and compares with price. 
        # If balance > price, vend beverage. Transition to State 2
        # If balance = price, vend beverage. Transition to state 6
        # If balance < price, display message and transition to State 4
        # If Eject is pressed, transition to state 5
        if balance > price: 
            balance = balance - price
            display_balance = balance/100
            print('DRINK VENDED. Balance: ${:.2f}'.format(display_balance))
            state = 2
        elif balance == price: 
            print('DRINK VENDED.')
            state = 6
        elif balance < price:
            state = 4
        else:
            pass
        
    elif state == 4:
        # Display error message and price of selected item
        # Transition back to S1
        display_balance = balance/100
        print('INSUFFICIENT FUNDS! Balance: ${:.2f}'.format(display_balance))
        state = 2
        
    elif state == 5: 
        # Eject change. Transition to state 5
        display_balance = balance/100
        print('Change returned: ${:.2f}'.format(display_balance))
        # print('Denominations:' + str(getChange(price, balance)))
        print( ('Denominations:\n'
                ' %d Pennies\n'
                ' %d Nickels\n'
                ' %d Dimes\n'
                ' %d Quarters\n'
                ' %d One dollar bills\n'
                ' %d Five dollar bills\n'
                ' %d Ten dollar bills\n'
                ' %d Twenty dollar bills') % getChange(price, balance))
        state = 6
        
    elif state == 6:
        # Print Have a nice day!
        # End of transaction. Clear inputs and return to state 0
        print('Have a nice day!')
        balance = 0
        display_balance = 0
        price = 0
        display_price = 0
        state = 1
        
    else:
        pass

        
        
        
        
        
        
        
        
        
        
        
