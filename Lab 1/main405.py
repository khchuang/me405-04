# -*- coding: utf-8 -*-
'''
@file main405.py

@mainpage 

@section sec_intro Introduction
Hello, welcome to my online portfolio! On here, I have documented my projects
and various coding assignments from ME 405, Introduction to Mechatronics.

Table of Contents:
    1. Labx01: Vendotron Simulation (\ref page_vend)
    2. Labx02: TBA
    3. Labx03: TBA
    4. Labx04: TBA
    5. Labx05: TBA

Source Code Repository: https://bitbucket.org/khchuang/me405-04/src/master/

@page page_vend Labx01: Vendotron Simulation Documentation

@section sec_sum1 Summary

@details    I wrote this code to simulate the functionality of a fictional vending 
            machine, the Vendotron. The code uses a finite state machine to 
            display print statements which represent the actions of a real vending
            machine. On start up, the user is greeted with this message: 
                Thirsty? Grab a cold drink today! 
                  ('c') Cuke      | $2.00
                  ('p') Popsi     | $4.25
                  ('s') Spryte    | $3.75
                  ('d') Dr.Pupper | $6.25
            The user can then enter "money" by pressing numbers on their keyboard:
            0-Penny, 1-Nickel, 2-Dime, 3-Quarter, 4-One Dollar, 5-Five Dollars,
            6-Ten Dollars, 7-Twenty Dollars. Each time a number is pressed, the 
            remaining balance is updated. 
            
            At any time the user can press 'c', 'p','s', or 'd' to make a drink selection. The code then compares the drink
            price to the balance and determines if the purchase can be made. If so,
            "DRINK VENDED" will display, indicating that a drink was dispensed successfully.
            If not, then it will display "INSUFFICIENT FUNDS" with your remaining balance. 
            Leftover balances can be "ejected" by pressing 'e' at any time during
            operation or additional selections can be made by inserting more money 
            into the machine. 
            
            The program will run indefinitely so use 'ctrl + c' to exit whenever
            you are finished with the Vendotron.

* Source code can be found here: https://bitbucket.org/khchuang/me405-04/src/master/Lab%201/me405L1.py
* Documentation can be found here: \ref me405L1

@seciton sec_transition1 State Transition Diagram

@details    Here's my state transition diagram to help illustrate how my code transitions
            between the different states. 

@image html Lab01.png


@author Kyle Chuang

@date January 19,2021
'''

