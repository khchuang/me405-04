sympref('FloatingPointOutput', true)
syms s
syms K1
syms K2
syms K3
syms K4
I = eye(4);
A = [0 0.1773 -5.2170 4.0111;
    0 -3.8358 112.8871 64.8295;
    1 0 0 0;
    0 1 0 0];
B = [32.4991;
    -703.2272;
    0;
    0];

% % Charlie's values
% A = [0 0.1770 -5.2160 4.0110;
%     0 -3.8350 112.8870 64.8290;
%     1 0 0 0;
%     0 1 0 0];
% B = [32.4990;
%     -703.2270;
%     0;
%     0];
K = [K1 K2 K3 K4];
A_CL = A-B*K;
P = det(s*I - A_CL);

d = [1 18 100];
r = roots(d);
a1 = [1 90];
a2 = [1 95];
out = conv(d, a1);
out1 = conv(out, a2);

eq1 = 32.4991*K1 - 703.2272*K2-3.8358 == 203;
eq2 = -0.0221*K1 + 32.4991*K3 - 703.2272*K4 - 59.6125 == 11980;
eq3 = -4.927e+3*K1 - 0.0072*K2 - 0.0221*K3 - 0.0035 == 172400;
eq4 = -4.927e+3*K3 - 0.0072*K4 - 791.0169 == 855000;
[C, D] = equationsToMatrix([eq1, eq2, eq3, eq4], K);
K_anl = linsolve(C,D)

%% verify result using place() command
p = [-95 -90 -9+4.3589i -9-4.3589i];
K_pl = place(A, B, p)