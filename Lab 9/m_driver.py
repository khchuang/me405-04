# -*- coding: utf-8 -*-
"""
@file       m_driver.py
@brief      Motor Driver
@details    This code sets up the motor driver for the ME405 board. This can be
            used to create two objects of this class that can independently control two
            separate motor drivers. 
            
            UPDATE FOR BALL BALANCING PROJECT:
                Included a saturation zone to account for each motor's deadzone
                when attached to the platform. Positive and negative bounds are 
                different because the weight of the inertia rods affects the amount of
                work the motor has to do to move the platform. 
                
@author     Kyle Chuang
@author     Enoch Nicholson
"""
import pyb, micropython


class MotorDriver:
    '''
    @brief This class implements a motor driver for the ME405 board.
    '''               
    def __init__ (self, motor, nSLEEP_pin, nFAULT_pin, IN1_pin, ch1, IN2_pin, ch2, timer):
        '''
        @details            Creates a motor driver by initializing GP10 pins and turning the motor off for safety.
        @param motor        A boolean object which represents whether the motor object being created is the primary motor. 
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin.
        @param nFAULT_pin   A pyb.Pin object to use as the fault pin. 
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
        @param ch1          A pyb.Timer.Channel object that selects the first channel
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2.
        @param ch2          A pyb.Timer.Channel object that tselects the second channel
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin
        '''
        
        # Creates an emergency buffer to store errors that are thrown inside ISR
        micropython.alloc_emergency_exception_buf(200)
        
        ## nSLEEP pin setup
        self.pinA15 = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.pinA15.low()

        ## Initialize timer and pins       
        self.pin1 = pyb.Pin(IN1_pin)
        self.pin2 = pyb.Pin(IN2_pin)
        
        self.tim = pyb.Timer(timer, freq=20000)
        
        self.tchA = self.tim.channel(ch1, pyb.Timer.PWM, pin=self.pin1)
        self.tchB = self.tim.channel(ch2, pyb.Timer.PWM, pin=self.pin2)
        
        ## nFAULT pin setup
        self.pinPB2 = pyb.Pin(nFAULT_pin, pyb.Pin.IN)
        
        ## Initialize nFAULT pin
        # self.fault_state = self.pinPB2.value()
        self.fault_state = 1
        
        ## Initializes the motor object
        self.motor = motor
        
        # If this is the primary motor, then initialize the external interrupt
        if self.motor == True:
            
            ## Detects when the the nFAULT pin is set to LOW and the callback function is called 
            self.extint = pyb.ExtInt (self.pinPB2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.myCallback)
    
    def enable(self):
        '''
        @brief      Sets the nSLEEP pin to high which enables the motors
        '''
        if self.fault_state == 0:
            self.CheckFault()
            
        else:
            # print('Enabling Motor')
            # print('enable')
            if self.motor == True:
                self.extint.disable()
                self.pinA15.high()
                pyb.delay(1)
                self.extint.enable()
            else:
                pass
            
    
    def disable(self):
        '''
        @brief      Sets the nSLEEP pin to low which disables the motors
        '''       
        self.pinA15.low()

        
    def set_duty(self, duty):
        '''
        @brief          Sets the duty cycle of the motor
        @details        This method sets the duty cycle to be sent to the motor to the
                        given motor to the given level. Positive values cause effort in
                        one direction, negative values in the opposite direction.
        @param duty     A signed integer holding the duty cycle of the PWM signal 
                        sent to the motor.
        '''    
        # print('set duty')
        self.CheckFault()
        self.tchA.pulse_width_percent(0)
        self.tchB.pulse_width_percent(0)
        
        # # Original code that does not account for motor friction:
        # if duty >= 0:
        #     self.tchA.pulse_width_percent(duty)
                
        # elif duty <0:
        #     self.tchB.pulse_width_percent(abs(duty))  
        
        
        # Deadzone tuning for motor 1
        if self.motor == True:
            if duty >= 0:
                if abs(duty) < 70:
                    self.tchA.pulse_width_percent(70)
                else:
                    self.tchA.pulse_width_percent(duty)
                
            elif duty <0:
                if abs(duty) < 50:
                    self.tchB.pulse_width_percent(50)
                else:
                    self.tchB.pulse_width_percent(abs(duty))   
        
        # Deadzone tuning for motor 2
        if self.motor == False:
            if duty >= 0:
                if abs(duty) < 50:
                    self.tchA.pulse_width_percent(50)
                else:
                    self.tchA.pulse_width_percent(duty)
                
            elif duty <0:
                if abs(duty) < 60:
                    self.tchB.pulse_width_percent(60)
                else:
                    self.tchB.pulse_width_percent(abs(duty))  
    
    def CheckFault(self):
        '''
        @brief      Checks pin PB2 to see if there is a fault present
        @details    This method checks the nFAULT pin for its value. If it is high,
                    then it will enable the motors and resume operation. If it is 
                    low, then it will print an error message and continuously check
                    the pin until it is set to high. 
        '''
        # print('check')
        self.fault_state = self.pinPB2.value()
        if self.fault_state == 0:
            while True:
                self.inpt = input('Error detected! Press "1" to retry: ')
                
                try:
                    val = int(self.inpt)
                    if val == 1:
                        self.fault_state = self.pinPB2.value()
                        if self.fault_state == 1:
                            print('Fault Cleared')
                            self.enable()
                            break
                except ValueError:
                    pass
                
                ## CODE THAT WILL AUTOMATICALLY CHECK FOR FAULT CLEARANCE AND CORRECT
                # while True:
                #     self.fault_state = self.pinPB2.value()
                #     if self.fault_state == 1:
                #         print('Fault Cleared')
                #         self.enable()
                #         break
                #     else:
                #         pass
            
        else:
            self.enable()

    # Callback function
    def myCallback(self, IQR_source):
        '''
        @brief      Callback function
        @details    This is the callback function that is used when the nFAULT pin
                    is set to low. It checks the instance of the motor for the primary
                    motor and will disable the motors accordingly. 
        '''
        self.fault_state = self.pinPB2.value()
        
        if self.motor == True:
            self.disable()
            
        else:
            pass
        
# Testing Motors and Encoders independently          
if __name__ == '__main__':
    
    # nSLEEP pin setup
    pinA15 = pyb.Pin.cpu.A15
    
    # nFAULT pin setup using the blue USER Button to test
    pinB2 = pyb.Pin.board.PB2
    
    # Motor 1 setup
    pinB4 = pyb.Pin.cpu.B4
    ch1 = 1
    pinB5 = pyb.Pin.cpu.B5
    ch2 = 2
    motor1 = True
    
    # Motor 2 setup
    pinB0 = pyb.Pin.cpu.B0
    ch3 = 3
    pinB1 = pyb.Pin.cpu.B1
    ch4 = 4
    motor2 = False
    
    # Create the timer object used for PWM generation
    tim = 3
    
    # Creates two objects which can independently control two separate motors
    m1 = MotorDriver(motor1, pinA15, pinB2, pinB4, ch1, pinB5, ch2, tim)
    m2 = MotorDriver(motor2, pinA15, pinB2, pinB0, ch3, pinB1, ch4, tim)
    
    # Enables the motor driver
    m1.enable()
    
    while True:
        # print(m1.fault_state)
        # Sets the motor 1 duty cycle to -50%
        m1.set_duty(20)
        
        # Sets the motor 2 duty cycle to 50%
        m2.set_duty(35)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
        