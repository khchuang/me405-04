# -*- coding: utf-8 -*-
"""
@file       balance.py
@brief      Balancing Ball Main File
@details    This code is Kyle's attempt at balancing the ball. It involves manually 
            zeroing the platform then inputting 0 to begin the balancing process.
            There is a failsafe where the motors will disable when there is no 
            input to the touchscreen, so the platform balancing will start once the ball
            is placed onto the platform.
            
            This code requires the encoder, motor, and touchscreen drivers made previously
            for successsful operation, as they provide inputs (ball position data from
            the touchscreen and the theta data from the encoders) to our control system. 
            The output of the control system is torque supplied to the motor shafts
            (T_x and T_y) which are converted to motor duty cycle values using a 
            constant gain conversion derived from the motor data sheet. The result
            is two motor objects that each control an axis of tilt for the platform
            which react to the location of the ball. 

@author     Kyle Chuang
@author     Enoch Nicholson
@date       March 15, 2021
"""
import pyb, utime
from pyb import Pin
from enc import EncoderDriver as e
from m_driver import MotorDriver as m
from tch_driver import touchscreen as t

# Encoder 1 setup
pinB6 = pyb.Pin.cpu.B6
pinB7 = pyb.Pin.cpu.B7
e1 = e(pinB6, pinB7, 4)

# Encoder 2 setup
pinC6 = pyb.Pin.cpu.C6
pinC7 = pyb.Pin.cpu.C7
e2 = e(pinC6, pinC7, 8)

# nSLEEP pin setup
pinA15 = pyb.Pin.cpu.A15

# nFAULT pin setup
pinB2 = pyb.Pin.board.PB2

# Motor 1 setup
pinB4 = pyb.Pin.cpu.B4
ch1 = 1
pinB5 = pyb.Pin.cpu.B5
ch2 = 2
motor1 = True

# Motor 2 setup
pinB0 = pyb.Pin.cpu.B0
ch3 = 3
pinB1 = pyb.Pin.cpu.B1
ch4 = 4
motor2 = False

# Create the timer object used for PWM generation
tim = 3

# Creates two objects which can independently control two separate motors
m1 = m(motor1, pinA15, pinB2, pinB4, ch1, pinB5, ch2, tim)
m2 = m(motor2, pinA15, pinB2, pinB0, ch3, pinB1, ch4, tim)

# Enables the motor driver
m1.enable()

# Creates the touchscreen object
xp = Pin.cpu.A0
xm = Pin.cpu.A6
yp = Pin.cpu.A7
ym = Pin.cpu.A1
w = 0.1 # [m]
l = 0.176 # [m]
xc = l/2 # [m]
yc = w/2 # [m]
ts = t(xp, xm, yp, ym, w, l, xc, yc)

# K Gain matrix
K1 = -100
K2 = -50
K3 = -500
K4 = -20

# Initial values of x and y
x_0 = 0
y_0 = 0

# Motor constants from data sheet
V_DC = 12 # Volts
R = 2.21 # Ohm
K_T = 13.8 #mNM/A
duty_conv = 100*R/(V_DC*K_T)

# Timing for delta t
start_time = utime.ticks_ms()

# Zero platform
while True:
    inpt = input('Manually level the platform. Press "0" when platform is ready: ')
    try: 
        # Checks to see if the value is 0. If not it will prompt the user again
        val = int(inpt)
        if val == 0:
            
            # Zeros the encoder positions
            theta_y_0 = e1.set_position(0)
            theta_x_0 = e2.set_position(0)
            break
        
    except ValueError:
        pass




# Ball Balancing 
while True:       
    # Time benchmark for delta t measurements
    curr_time = utime.ticks_ms() 
    delta_t = utime.ticks_diff(curr_time, start_time) # [s]
    
    # Coordinates of touchscreen input
    P = ts.coord()
    
    # Updates the encoder position
    e1.update()
    e2.update()
    
    # If the ball is on the platform:
    if P[2] == 0:     
        
        # Theta_y read from Encoder 1 (for MOTOR 2)
        theta_y_1 = 2*3.14*e1.get_position()/4000 # [Radians]
        thetadot_y = (theta_y_1 - theta_y_0)/ delta_t # [Radians/sec]
        theta_y_0 = theta_y_1 # [Radians]
        
        # X read from Touchscreen driver (for MOTOR 2)
        x_1 = P[0] # [m]
        xdot = (x_1 - x_0)/delta_t # [m/s]
        x_0 = x_1 # [m]
        
        # Torque_x calculation (for MOTOR 2)
        T_x = -K1*xdot - K2*thetadot_y - K3*x_1 - K4*theta_y_1 # [N-m]
        Duty_x = int(-1*duty_conv*T_x) #[%]
        m2.set_duty(Duty_x)
        
        # # Motor 2 Testing
        # print('Motor 2 duty cycle: ' + str(Duty_x))
        # print('{:},{:},{:},{:}, Duty_2 = {:} \r\n'.format(xdot, thetadot_y, x_1, theta_y_1, Duty_x))
        
        
        # Theta_x read from Encoder 2 (for MOTOR 1)
        theta_x_1 = 2*3.14*e2.get_position()/4000 # [Radians]
        thetadot_x = (theta_x_1 - theta_x_0)/ delta_t # [Radians/sec]
        xdot = (x_1 - x_0)/delta_t # [Radians]
        
        # Y position read from Touchscreen driver (for MOTOR 1)
        y_1 = P[1] # [m]
        ydot = (y_1 - y_0)/delta_t # [m/s]
        y_0 = y_1 # [m]
        
        # Torque_y calculation(for MOTOR 1)
        T_y = -K1*ydot - K2*thetadot_x - K3*y_1 - K4*theta_x_1 # [N-m]
        Duty_y = int(duty_conv*T_y) # [%]
        m1.set_duty(Duty_y)
        
        # # Motor 1 Testing 
        # print('Motor 1 duty cycle: ' + str(Duty_y))
        # print('{:},{:},{:},{:}, Duty_1 = {:} \r\n'.format(ydot, thetadot_x, y_1, theta_x_1, Duty_y))


    # Disables the motors if there is no input
    else:
        m1.disable()
    
    # Updates time values 
    start_time = curr_time
    


















